import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, pipe } from 'rxjs';

import { filter, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  url: string = 'https://randomuser.me/api/'

  constructor(public http: HttpClient) { }

  obtenerUsuario(): Observable<any>{
    return this.http.get<any>(this.url) //el get viene de la aplicacion_ buscar que es un observable,
  }
  obtenerUsuarioMasculino(): Observable<any>{
    return this.http.get<any>(this.url).
    pipe(
      filter(
        usuario => usuario['results'][0].gender != 'male'//solamente va fitrar los mas
      )
    )
  }

  ObtenerdistintoDeCanada(): Observable<any>{
    return this.http.get<any>(this.url).
    pipe(
      filter(
        usuario => usuario['results'][0].location ['country'] != 'Canada'//solamente va fitrar los demas
      )
    )
  }

  ObtenerCantidadDeElementos(): Observable<any>{
    return this.http.get<any>(this.url).
    pipe(
      take(0)//se le dice cuantos elementos queremos
    );
  }

  obtenerFoto(): Observable<any> {
    return this.http.get<any>(this.url).
    pipe(
      map(resp  => {
        //console.log(resp);
        return resp ['results'].map((usuario: any) =>{
          //return usuario;
          return {
            name: usuario.name,
            email: usuario.email,
            login: usuario.login,
            picture: usuario.picture,
          }
        });
        
      })
    );
  }

}

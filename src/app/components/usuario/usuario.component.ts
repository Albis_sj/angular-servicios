import { Component, OnInit } from '@angular/core';
import { Subscriber } from 'rxjs';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  usuario: any; 
  usuario2: any;
  usuario3: any;
  usuario4:any;
  femenino: boolean;
  nacionalidad: boolean;
  lugar: string;

  constructor(private servicioUsuario: UsuarioService) { 
    this.femenino = false;
    this.nacionalidad = false;
    this.lugar = '';
  }

  ngOnInit(): void {
    this.servicioUsuario.obtenerUsuario().subscribe({
      next: user => { //obtiene el valor que tiene el observable
        console.log(user); //imprime el json
        this.usuario = user['results'][0] //aqui sacamos lo que queremos del array de jsons

        console.log(this.usuario);        
        
      },
      error: error => { //si hay un error viene aca
        console.log(error);
      },
      complete: () => {
        console.log('Solicitud Completa');
      }
    });
  }

  showFemale(): void{
    this.servicioUsuario.obtenerUsuarioMasculino().subscribe({
      next: user => {
      console.log(user);
      this.usuario2 = user ['results'][0];
      this.femenino = true;
      }, 
      error: error => {
        console.log(error);
        
      }
    })
  }

  ShowDistintos(): void {
    this.servicioUsuario.ObtenerdistintoDeCanada().subscribe({
      next: user =>{
        console.log(user);
        this.usuario = user ['results'][0];
        this.nacionalidad = true;        
      },
      error: error => {
        console.log(error);
      }
    })
  }



  MostrarElementos():void{
    this.servicioUsuario.ObtenerCantidadDeElementos().subscribe({
      next: user => {
        console.log(user);
      },
      error: error => {
        console.log(error);
      }
    });
  }

  MostrarFoto(): void {
    this.servicioUsuario.obtenerFoto().subscribe({
      next: user => {
        console.log(user);
        this.usuario3 = user [0];
      },
      error: error => {
        console.log(error);
        
      }
    })
  }


  mostrarDatos(): void{
    this.servicioUsuario.obtenerFoto().subscribe({
      next: user => {
        console.log(user);
        this.usuario4 = user [0];
      },
      error: error => {
        console.log(error);
      }
    })
  }
}
